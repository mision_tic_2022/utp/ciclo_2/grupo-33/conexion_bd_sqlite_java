import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        conectarBD();
    }

    public static void conectarBD() {
        try {
            // Crear conexión a base de datos
            Connection conexion = DriverManager.getConnection("jdbc:sqlite:hr.db");
            if (conexion != null) {
                System.out.println("Conexión exitosa a la base de datos");
                // Crear objeto para la ejecución de consultas sql
                Statement st = conexion.createStatement();
                // selectAllEmployees(st);
                // insertJob(conexion);
                selectAllJobs(st);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void selectAllEmployees(Statement st) throws SQLException {
        // Ejecutar una consulta/query
        String query = "SELECT * FROM employees";
        ResultSet result_1 = st.executeQuery(query);
        String info = "";
        // Iterar result_1
        while (result_1.next()) {
            String nombre = result_1.getString("first_name");
            String apellido = result_1.getString("last_name");
            info += "Nombre: " + nombre;
            info += "\nApellido: " + apellido;
            info += "\n--------------------------\n";
        }
        System.out.println(info);
    }

    public static void insertJob(Connection conexion) throws SQLException {
        String job_title = "Deleveloper";
        int min_salary = 2000;
        int max_salary = 15000;
        // Preparar consulta SQL
        PreparedStatement pst = conexion
                .prepareStatement("INSERT INTO jobs(job_title, min_salary, max_salary) VALUES(?, ?, ?)");
        // Setear los valores en la consulta
        pst.setString(1, job_title);
        pst.setInt(2, min_salary);
        pst.setInt(3, max_salary);
        // Ejecutar consulta
        pst.executeUpdate();
    }

    public static void selectAllJobs(Statement st) throws SQLException {
        ResultSet result = st.executeQuery("SELECT * FROM jobs");
        while (result.next()) {
            System.out.println("Job: " + result.getString("job_title"));
        }
    }

}
